package ru.eltech.observer.data.local.model


import com.google.gson.annotations.SerializedName

data class CompanyDto(
    @SerializedName("bs")
    val bs: String,
    @SerializedName("catchPhrase")
    val catchPhrase: String,
    @SerializedName("name")
    val name: String
)