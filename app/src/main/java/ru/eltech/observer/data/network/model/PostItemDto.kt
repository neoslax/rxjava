package ru.eltech.retrofitreciver.data.network.model


import com.google.gson.annotations.SerializedName

data class PostItemDto(
    @SerializedName("body")
    val body: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("userId")
    val userId: Int
)