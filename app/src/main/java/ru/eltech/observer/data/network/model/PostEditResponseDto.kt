package ru.eltech.retrofitreciver.data.network.model

data class PostEditResponseDto(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)