package ru.eltech.observer.data.local.model


import com.google.gson.annotations.SerializedName

data class UsersItemDto(
    @SerializedName("address")
    val address: AddressDto,
    @SerializedName("company")
    val company: CompanyDto,
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("website")
    val website: String
)