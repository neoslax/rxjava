package ru.eltech.observer.data.local

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Observable
import ru.eltech.observer.R
import ru.eltech.observer.data.local.model.UsersItemDto

object LocalData {

    fun getLocalData(context: Context): Observable<List<UsersItemDto>> {
        val inputStream = context.resources.openRawResource(R.raw.json_mock_data)
        val text = inputStream.bufferedReader().use {
            it.readText()
        }
        val itemType = object : TypeToken<List<UsersItemDto>>() {}.type
        val data = Gson().fromJson<List<UsersItemDto>>(text, itemType)
        return Observable.just(data)
    }
}