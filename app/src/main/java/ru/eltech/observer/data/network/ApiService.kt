package ru.eltech.observer.data.network

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*
import ru.eltech.retrofitreciver.data.network.model.PostEditBodyDto
import ru.eltech.retrofitreciver.data.network.model.PostEditResponseDto
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

interface ApiService {

    @GET("posts")
    fun getAllPosts(): Observable<List<PostItemDto>>

}