package ru.eltech.observer

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.eltech.observer.data.local.LocalData
import ru.eltech.observer.databinding.FragmentObserverBinding
import ru.eltech.observer.data.network.ApiFactory
import java.util.concurrent.TimeUnit


class ObserverFragment : Fragment() {

    private var _binding: FragmentObserverBinding? = null
    private val binding: FragmentObserverBinding
        get() = _binding ?: throw RuntimeException("FragmentObserverBinding == null")

    private val compositeDisposable by lazy {
        CompositeDisposable()
    }

    private val api = ApiFactory.apiService

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentObserverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnObserve.setOnClickListener(::getRESTData)
    }

    private fun getLocalData(v: View) {
        LocalData.getLocalData(requireContext())
            .flatMapIterable { it }
            .concatMap { Observable.just(it).delay(250, TimeUnit.MILLISECONDS) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                binding.tvObserverData.text = "${it.name} \n ${it.id}"
            }
    }

    private fun getRESTData(v: View) {
        api.getAllPosts()
            .flatMapIterable { it }
            .concatMap { Observable.just(it).delay(250, TimeUnit.MILLISECONDS) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                binding.tvObserverData.text = "${it.title} \n ${it.id}"
            }, {
                binding.tvObserverData.text = "error"
            }, {
                binding.tvObserverData.text = "data ran out"
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        compositeDisposable.dispose()
    }

}