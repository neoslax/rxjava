package ru.eltech.observer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.eltech.observer.data.local.LocalData
import ru.eltech.observer.data.network.ApiFactory
import ru.eltech.observer.databinding.FragmentSettingsBinding
import java.util.concurrent.TimeUnit

class SettingFragment : Fragment() {

    private var _binding: FragmentSettingsBinding? = null
    private val binding: FragmentSettingsBinding
        get() = _binding ?: throw RuntimeException("FragmentSettingsBinding == null")

    private val compositeDisposable by lazy {
        CompositeDisposable()
    }

    private val data by lazy {
        (requireActivity() as MainActivity)
    }

    private val api = ApiFactory.apiService

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnJson.setOnClickListener(::getLocalData)
        binding.btnRest.setOnClickListener(::getRESTData)
    }

    private fun getLocalData(v: View) {
        LocalData.getLocalData(requireContext())
            .flatMapIterable { it }
            .concatMap { Observable.just(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {},{},{
                binding.textView.text = "json ready"
            })

    }

    private fun getRESTData(v: View) {
        api.getAllPosts()
            .flatMapIterable { it }
            .concatMap { Observable.just(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            }, {

            }, {
                binding.textView.text = "rest ready"
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}